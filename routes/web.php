<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\RouteHelperController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//consumers
Route::get('/', function () {
    return view('consumers.welcome');
})->name('welcome');

Route::get('showtimes/{movieId?}', [RouteHelperController::class, 'getShowtimes'])->name('showtimes');


Auth::routes();

// admin routes protected by Admin Middleware
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('movies', \App\Http\Controllers\Admin\MoviesController::class);
    Route::resource('movies-showtimes', \App\Http\Controllers\Admin\MovieShowtimesController::class);
    Route::get('edit-movie-showtimes/{movieId}', [\App\Http\Controllers\Admin\MoviesController::class, 'addEditShowtimes'])->name('edit-movie-showtimes');
});
