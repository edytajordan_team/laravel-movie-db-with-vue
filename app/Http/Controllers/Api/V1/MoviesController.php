<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$movies = Movie::all();
        $movies = Movie::with('showtimes')->with('reviews')->get();
        // todo return Resource to control what is returned
        //return MovieResource::collection($movie->with('showtimes')->with('reviews')->get());
        //
        //return response()->json($movies->with('showtimes')->with('reviews')->get());
        //todo this Trait will make all API calls uniformed
        return $this->successResponse($movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //$movie = Movie::findOrFail($id);
        $movie = Movie::with('showtimes')->with('reviews')->where('id', $id)->get();
        return $this->successResponse($movie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFeaturedMovie(Request $request){
        $movie = Movie::where('featured', true)->get();
        return $this->successResponse($movie);
    }
}
