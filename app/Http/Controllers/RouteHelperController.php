<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteHelperController extends Controller
{
    public function getShowtimes($movieId = ''){
        return view('consumers.showtimes', [
            'movieId' => $movieId,
        ]);
    }
}
