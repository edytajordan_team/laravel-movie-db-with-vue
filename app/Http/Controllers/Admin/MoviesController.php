<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMovie;
use App\Models\Movie;
use App\Models\MovieShowtime;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('theater-admin.movies-management');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('theater-admin.movies-management-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovie $request)
    {
        $validatedData = $request->validated();
        //dd($validatedData);

        //todo FILES
        $imageName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('images'), $imageName);

        $validatedData['poster'] = $imageName;
        $movie = Movie::create($validatedData);

        $request->session()->flash('status', 'Movie was created!');
        return redirect()->route('movies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param  Request  $request
     * @param $id
     */
    public function addEditShowtimes($movieId){

        $showtimes = MovieShowtime::where('movie_id', $movieId)->get();
        $movie = Movie::findOrFail($movieId);
        //dd($showtimes);

        return view('theater-admin.movies-management-edit-showtime', [
            'movieId'   => $movieId,
            'movie'     => $movie,
            'showtimes' => $showtimes,
        ]);
    }
}
