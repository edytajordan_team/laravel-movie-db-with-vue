<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'poster', 'rating'];

    public function showtimes(){
        return $this->hasMany(MovieShowtime::class);
    }

    public function reviews(){
        return $this->hasMany(MovieReview::class);
    }
}
