const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
const postcssImport = require('postcss-import');
const postcssCustomProperties = require('postcss-custom-properties');
const autoprefixer = require('autoprefixer');
const postcssNested = require('postcss-nested');
const tailwindConfig = require('./tailwind.config');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application.
 |
 */

// JS
mix.combine(
    [
        'resources/js/libs/script.js',
    ],
    'public/js/script.js',
);

// CSS
mix.postCss('resources/css/app.css', 'public/css', [
    postcssImport(),
    tailwindcss({
        ...tailwindConfig,
        purge: {
            enabled: mix.inProduction(),
            content: [
                './resources/**/*.blade.php',
                './resources/**/*.js',
                './resources/**/*.vue',
                'src/**/*.html',
            ],
            options: {
                safelist: ['dark', 'carousel', 'carousel__icon', 'carousel__track', 'carousel__viewport',
                    'carousel__pagination', 'carousel__pagination-button', 'carousel__pagination-button--active',
                    'carousel__slide', 'carousel__prev', 'carousel__next'],
            },
        },
    }),
    postcssNested(),
    postcssCustomProperties(),
    autoprefixer(),
]);

mix.js('resources/js/app.js', 'public/js')
    .vue({ version: 3 })
    .webpackConfig((webpack) => ({
        plugins: [
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
            }),
        ],
        stats: {
            children: true,
        },
    }))
    .copy('resources/images/', 'public/images')
    .version()
    .sourceMaps();
