const root = document.documentElement;
const menuToggler = document.getElementById('menuToggler');
const menuBar = document.querySelector('.menu-bar');

// Toggle Menu
function toggleMenu() {
    if (menuBar.classList.contains('menu-hidden')) {
        root.classList.remove('menu-hidden');
        menuBar.classList.remove('menu-hidden');
    } else {
        root.classList.add('menu-hidden');
        menuBar.classList.add('menu-hidden');
    }
}

// Viewport Width
// Define our viewportWidth variable
let viewportWidth;

// Set/update the viewportWidth value
const setViewportWidth = () => {
    viewportWidth = window.innerWidth || document.documentElement.clientWidth;
};

// Watch the viewport width
const watchWidth = () => {
    const sm = 640;
    const md = 768;
    const lg = 1024;
    const xl = 1280;

    // Hide Menu Detail
    const hideMenuDetail = () => {
        menuBar.querySelectorAll('.menu-detail.open').forEach((menuDetail) => {
            hideOverlay();

            if (!menuBar.classList.contains('menu-wide')) {
                menuDetail.classList.remove('open');
            }
        });
    };

    // Hide Sidebar
    const hideSidebar = () => {
        const sidebar = document.querySelector('.sidebar');

        if (!sidebar) return;

        if (sidebar.classList.contains('open')) {
            sidebar.classList.remove('open');
            hideOverlay();
        }
    };

    if (viewportWidth < sm) {
        if (!menuBar) return;

        const openMenu = menuBar.querySelector('.menu-detail.open');

        if (!openMenu) {
            menuBar.classList.add('menu-hidden');
            document.documentElement.classList.add('menu-hidden');
            hideMenuDetail();
        }
    }

    if (viewportWidth > sm) {
        if (!menuBar) return;

        menuBar.classList.remove('menu-hidden');
        document.documentElement.classList.remove('menu-hidden');
    }

    if (viewportWidth > lg) {
        hideSidebar();
    }
};

// Set our initial width
setViewportWidth();
watchWidth();

// On resize events, recalculate
window.addEventListener(
    'resize',
    () => {
        setViewportWidth();
        watchWidth();
    },
    false,
);