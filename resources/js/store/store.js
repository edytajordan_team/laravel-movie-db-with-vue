import { createStore } from 'vuex';

// create a store instance
const store = createStore({

    // import modules, make sure they're namespaced to avoid naming clashes
    modules: {

    },
    // state will be merged here from imported modules
});

export default store;
