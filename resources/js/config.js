import { createApp } from 'vue';
import store from './store/store';

// create app instance
const app = createApp({});

// components
// UI Theme 2021
app.component('main-menu', require('./components/ui-theme-2021/MainMenu.vue').default);
app.component('user-menu', require('./components/ui-theme-2021/UserMenu.vue').default);
app.component('dark-mode-toggler', require('./components/ui-theme-2021/DarkModeToggler.vue').default);

//movies
app.component('movie-slideshow', require('./components/Movies/MovieSlideshow.vue').default);
app.component('movie', require('./components/Movies/Movie.vue').default);
app.component('movie-featured', require('./components/Movies/MovieFeatured.vue').default);
app.component('movies-filtered', require('./components/Movies/MoviesFiltered.vue').default);
app.component('movie-checkout', require('./components/Movies/MovieCheckout.vue').default);
//movies admin
app.component('movie-management', require('./components/Movies/Admin/MovieManagement.vue').default);

// global directives
// Register a global custom directive called `v-focus`
app.directive('focus', {
    // When the bound element is mounted into the DOM...
    mounted(el) {
        // Focus the element
        el.focus();
    },
});

app.config.devtools = process.env.NODE_ENV;

// use statements
app.use(store);

// this should always be the last
app.mount('#app');
