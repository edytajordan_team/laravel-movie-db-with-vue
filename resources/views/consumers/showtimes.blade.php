@extends('layouts.consumer')

@section('content')
    <div class="container">
        <movies-filtered :movie-id="'{{ $movieId }}'"></movies-filtered>
    </div>
@endsection