@extends('layouts.consumer')

@section('featured-movie')
    <movie-featured></movie-featured>
@endsection

@section('content')
    <movie-slideshow></movie-slideshow>
@endsection
