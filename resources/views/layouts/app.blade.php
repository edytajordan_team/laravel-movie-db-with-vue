<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Movie DB') }} @hasSection('title') · @endif @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('style')
</head>


<body class=" @env('local') debug-screens @endenv ">

    <div class="body" id="app">
        @include('layouts.partials.navigation')
        <!-- Workspace -->
        <main class="workspace overflow-hidden">
            <section>
                @yield('content')
            </section>
        </main>
    </div>
    <!-- end of APP -->

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/script.js') }}"></script>
    @yield('scripts')

</body>
</html>
