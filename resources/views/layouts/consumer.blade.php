<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Movie DB') }} @hasSection('title') · @endif @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('style')
</head>

<body class="antialiased bg-black">
    <div id="app">
        <header class="@if(Route::currentRouteName() === 'welcome') header @endif relative lg:overflow-hidden">
            <nav class="md:p-3 py-1 px-3 z-10">
                <div class="container mx-auto md:flex block flex-wrap items-center justify-start">
                    <div class="flex-1 flex items-center justify-between">
                        <a class="text-white flex items-center" href="{{ route('welcome') }}">
                            <span class="text-3xl font-light">CINE</span>
                            <img src="{{ mix('images/film.svg') }}"/>
                            <span class="text-3xl font-ligh">A</span>
                        </a>
                        <button data-menu-toggle class="md:hidden block pr-1">
                            <svg class="fill-current text-white w-6 h-6"
                                 xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 viewBox="0 0 20 20"><title>menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
                        </button>
                    </div>
                    <ul data-menu class="md:flex hidden flex-wrap items-center justify-start text-white">
                        @if(Route::currentRouteName() !== 'welcome')
                            <li class="md:px-6 py-2">
                                <a href="{{ route('welcome') }}" class="text-brand">Home</a>
                            </li>
                        @endif
                        <li class="md:px-6 py-2">
                            <a href="#">See a Movie</a>
                        </li>
                        <li class="md:px-6 py-2">
                            <a href="#">Our Theater</a>
                        </li>

                        <li>
                            <div class="flex-1 px-2 flex justify-center lg:ml-6 lg:justify-end">
                                <div class="max-w-lg w-full lg:max-w-xs">
                                    <label for="search" class="sr-only">Search</label>
                                    <div class="relative text-gray-300 focus-within:text-gray-400">
                                        <div class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center">
                                            <!-- Heroicon name: solid/search -->
                                            <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#ffffff" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                                            </svg>
                                        </div>
                                        <input id="search"
                                               class="block w-full py-2 pl-10 pr-3 rounded-md
                                        leading-5 text-white placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-offset-2
                                        focus:ring-offset-indigo-600 focus:ring-gray-600 focus:border-gray-500 sm:text-sm bg-gray-900 border-0 opacity-90"
                                               placeholder="Search"
                                               type="search"
                                               name="search">
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="md:px-6 py-2">
                            <a class="flex items-center text-brand font-bold hover:text-white" href="{{ route('showtimes') }}">
                                    <i class="icon icon_showtimes" id="showtimes_icon" alt="Showtimes" style="background-image: none;">
                                        <svg focusable="false" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="icon_showtimes"
                                             viewBox="0 0 45 45" preserveAspectRatio="xMidYMid meet" fill="#ff5900" width="35px" height="35px">
                                            <path d="M15.957 33.601a.648.648 0 0 0 .451-.197l8.845-8.817a.648.648 0 0 0 0-.901l-8.45-8.451a.648.648 0 0 0-.902 0l-8.873 8.789a.648.648 0 0 0 0 .901l8.45 8.451a.648.648 0 0 0 .48.225zm.226-17.07l7.605 7.606-7.83 7.915-7.606-7.606zm4.93-1.493a.704.704 0 0 0 .506-1.211l-.957-.958a.717.717 0 0 0-1.014 1.014l.957.958a.704.704 0 0 0 .507.197zm5.774 5.803a.704.704 0 0 0 .563-1.183l-1.014-1.014a.717.717 0 1 0-1.014 1.014l.958.957a.704.704 0 0 0 .507.226zm-2.873-2.902a.704.704 0 0 0 .62-1.098l-1.1-1.099a.748.748 0 1 0-1.013 1.099l.986.901a.704.704 0 0 0 .507.197zm12.93 11.212a.704.704 0 0 0-.705.704v1.352a.718.718 0 0 0 1.437 0v-1.352a.704.704 0 0 0-.732-.704zm0 4.084a.704.704 0 0 0-.705.704v1.353a.718.718 0 1 0 1.437 0v-1.353a.704.704 0 0 0-.732-.704zm0-8.197a.704.704 0 0 0-.705.704v1.38a.718.718 0 1 0 1.437 0V25.77a.704.704 0 0 0-.732-.732z"></path>
                                            <path d="M44.352 28.108A.648.648 0 0 0 45 27.46v-6.197a.648.648 0 0 0-.648-.648H33.986l1.915-1.971a.648.648 0 0 0 0-.902l-3.774-3.86-.507-.506a.648.648 0 0 0-.902 0 2.59 2.59 0 1 1-3.662-3.662.648.648 0 0 0 0-.902L22.69 4.446a.648.648 0 0 0-.873 0L.182 26.08a.648.648 0 0 0 0 .902l4.367 4.366a.648.648 0 0 0 .901 0 2.57 2.57 0 1 1 3.634 3.634.648.648 0 0 0 0 .901l4.282 4.366a.62.62 0 0 0 .394.17v.281h30.592a.648.648 0 0 0 .648-.648v-6.31a.648.648 0 0 0-.648-.507 2.563 2.563 0 0 1 0-5.127zm-33.944 7.296a3.86 3.86 0 0 0-5.493-5.38l-3.38-3.493 15.38-15.324.789.732a.717.717 0 1 0 1.014-1.014l-.789-.789 4.31-4.338L25.76 9.32a3.86 3.86 0 0 0 5.352 5.353l3.409 3.408-4.254 4.395-.957-.958a.717.717 0 1 0-1.014 1.014l.957.958-15.352 15.408zm9.211-.394l9.043-9.042h3.324v10.084H19.619v-1.07zm24.17-.535v4.901h-6.113v-1.324a.718.718 0 0 0-1.437 0v1.324H15.197l3.127-3.099v.282a.648.648 0 0 0 .647.648h13.634a.648.648 0 0 0 .648-.648V25.291a.648.648 0 0 0-.648-.647h-2.647l2.816-2.817h3.522v1.21a.718.718 0 0 0 1.436 0v-1.126h6.057v4.958a3.86 3.86 0 0 0 0 7.577z"></path>
                                        </svg>
                                    </i>
                                    <span class="ml-2">Showtimes</span>
                                </a>
                        </li>

                    </ul>
                </div>
            </nav>
            @yield('featured-movie')
        </header>

        <section>
            @yield('content')
        </section>

        <!-- todo our footer -->
        <div class="container mx-auto py-5 px-4 text-center">
            <div class="flex justify-center">
                <a href="#" class="mx-2">
                    <svg class="fill-current text-white opacity-50 hover:opacity-75 w-10 h-10"
                         xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <title>instagram</title>
                        <path d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.333 4.14.63c-.789.306-1.459.717-2.126 1.384S.935 3.35.63 4.14C.333 4.905.131 5.775.072 7.053.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.261 2.148.558 2.913a5.885 5.885 0 0 0 1.384 2.126A5.868 5.868 0 0 0 4.14 23.37c.766.296 1.636.499 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558a5.898 5.898 0 0 0 2.126-1.384 5.86 5.86 0 0 0 1.384-2.126c.296-.765.499-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.149-.558-2.913a5.89 5.89 0 0 0-1.384-2.126A5.847 5.847 0 0 0 19.86.63c-.765-.297-1.636-.499-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.071 1.17.055 1.805.249 2.227.415.562.217.96.477 1.382.896.419.42.679.819.896 1.381.164.422.36 1.057.413 2.227.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.061 1.17-.256 1.805-.421 2.227a3.81 3.81 0 0 1-.899 1.382 3.744 3.744 0 0 1-1.38.896c-.42.164-1.065.36-2.235.413-1.274.057-1.649.07-4.859.07-3.211 0-3.586-.015-4.859-.074-1.171-.061-1.816-.256-2.236-.421a3.716 3.716 0 0 1-1.379-.899 3.644 3.644 0 0 1-.9-1.38c-.165-.42-.359-1.065-.42-2.235-.045-1.26-.061-1.649-.061-4.844 0-3.196.016-3.586.061-4.861.061-1.17.255-1.814.42-2.234.21-.57.479-.96.9-1.381.419-.419.81-.689 1.379-.898.42-.166 1.051-.361 2.221-.421 1.275-.045 1.65-.06 4.859-.06l.045.03zm0 3.678a6.162 6.162 0 1 0 0 12.324 6.162 6.162 0 1 0 0-12.324zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405a1.441 1.441 0 0 1-2.88 0 1.44 1.44 0 0 1 2.88 0z"></path></svg>
                </a>
                <a href="#" class="mx-2">
                    <svg class="fill-current text-white opacity-50 hover:opacity-75 w-10 h-10" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>vimeo</title><path d="M23.977 6.416c-.105 2.338-1.739 5.543-4.894 9.609-3.268 4.247-6.026 6.37-8.29 6.37-1.409 0-2.578-1.294-3.553-3.881L5.322 11.4C4.603 8.816 3.834 7.522 3.01 7.522c-.179 0-.806.378-1.881 1.132L0 7.197a315.065 315.065 0 0 0 3.501-3.128C5.08 2.701 6.266 1.984 7.055 1.91c1.867-.18 3.016 1.1 3.447 3.838.465 2.953.789 4.789.971 5.507.539 2.45 1.131 3.674 1.776 3.674.502 0 1.256-.796 2.265-2.385 1.004-1.589 1.54-2.797 1.612-3.628.144-1.371-.395-2.061-1.614-2.061-.574 0-1.167.121-1.777.391 1.186-3.868 3.434-5.757 6.762-5.637 2.473.06 3.628 1.664 3.493 4.797l-.013.01z"></path></svg>
                </a>
                <a href="#" class="mx-2">
                    <svg class="fill-current text-white opacity-50 hover:opacity-75 w-10 h-10" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>youtube</title><path class="a" d="M23.495 6.205a3.007 3.007 0 0 0-2.088-2.088c-1.87-.501-9.396-.501-9.396-.501s-7.507-.01-9.396.501A3.007 3.007 0 0 0 .527 6.205a31.247 31.247 0 0 0-.522 5.805 31.247 31.247 0 0 0 .522 5.783 3.007 3.007 0 0 0 2.088 2.088c1.868.502 9.396.502 9.396.502s7.506 0 9.396-.502a3.007 3.007 0 0 0 2.088-2.088 31.247 31.247 0 0 0 .5-5.783 31.247 31.247 0 0 0-.5-5.805zM9.609 15.601V8.408l6.264 3.602z"></path></svg>
                </a>
                <a href="#" class="mx-2">
                    <svg class="fill-current text-white opacity-50 hover:opacity-75 w-10 h-10" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>facebook</title><path d="M22.676 0H1.324C.593 0 0 .593 0 1.324v21.352C0 23.408.593 24 1.324 24h11.494v-9.294H9.689v-3.621h3.129V8.41c0-3.099 1.894-4.785 4.659-4.785 1.325 0 2.464.097 2.796.141v3.24h-1.921c-1.5 0-1.792.721-1.792 1.771v2.311h3.584l-.465 3.63H16.56V24h6.115c.733 0 1.325-.592 1.325-1.324V1.324C24 .593 23.408 0 22.676 0"></path></svg>
                </a>
                <a href="#" class="mx-2">
                    <svg class="fill-current text-white opacity-50 hover:opacity-75 w-10 h-10" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>twitter</title><path d="M23.954 4.569a10 10 0 0 1-2.825.775 4.958 4.958 0 0 0 2.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 0 0-8.384 4.482C7.691 8.094 4.066 6.13 1.64 3.161a4.822 4.822 0 0 0-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 0 1-2.228-.616v.061a4.923 4.923 0 0 0 3.946 4.827 4.996 4.996 0 0 1-2.212.085 4.937 4.937 0 0 0 4.604 3.417 9.868 9.868 0 0 1-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 0 0 7.557 2.209c9.054 0 13.999-7.496 13.999-13.986 0-.209 0-.42-.015-.63a9.936 9.936 0 0 0 2.46-2.548l-.047-.02z"></path></svg>
                </a>
            </div>
        </div>
        <footer class="container mx-auto text-white py-6 md:px-16 px-4">
            <div class="md:pl-3 md:border-l border-white mb-6">
                <div class="flex w-full justify-between">
                    <div class="section">
                        <h3 class="mb-3">Our Company</h3>
                        <ul class="u-listUnstyled" role="navigation">
                            <li><a text="Our Brands" class="Link" href="#">Our Brands</a></li>
                            <li><a text="Contact Us" class="Link" href="#">Contact Us</a></li>
                            <li><a text="Corporate Information" class="Link" href="#">Corporate Information</a></li>
                            <li><a text="AMC Investor Connect" class="Link" href="#">AMC Investor Connect</a></li>
                            <li><a text="Investor Relations" href="#" class="Link" target="_blank" rel="noopener noreferrer">Investor Relations</a></li>
                            <li><a text="Media Center" class="Link" href="#">Media Center</a></li>
                            <li><a text="Careers" class="Link" href="#">Careers</a></li>
                            <li><a text="Associate Portal" class="Link" href="#">Associate Portal</a></li>
                            <li><a text="AMC Privacy Policy" class="Link" href="#">AMC Privacy Policy</a></li>
                            <li><a text="Terms &amp; Conditions" class="Link" href="#">Terms &amp; Conditions</a></li>
                        </ul>
                    </div>
                    <div class="section">
                        <h3 class="mb-3">Movies</h3>
                        <ul class="u-listUnstyled" role="navigation">
                            <li><a text="Movies" class="Link" href="#">Movies</a></li>
                            <li><a text="Theatres" class="Link" href="#">Theatres</a></li>
                            <li><a text="Ratings Information" class="Link" href="#">Ratings Information</a></li>
                            <li><a text="Dolby Cinema at AMC" class="Link" href="#">Dolby Cinema at AMC</a></li>
                            <li><a text="IMAX at AMC" class="Link" href="#">IMAX at AMC</a></li>
                            <li><a text="PRIME at AMC" class="Link" href="#">PRIME at AMC</a></li>
                            <li><a text="RealD 3D" class="Link" href="#">RealD 3D</a></li>
                            <li><a text="BigD at AMC" class="Link" href="#">BigD at AMC</a></li>
                        </ul>
                    </div>
                    <div class="section">
                        <h3 class="mb-3">Programming</h3>
                        <ul class="u-listUnstyled" role="navigation">
                            <li><a text="Private Theatre Rentals" class="Link" href="#">Private Theatre Rentals</a></li>
                            <li><a text="AMC Artisan Films" class="Link" href="#">AMC Artisan Films</a></li>
                            <li><a text="$5 Fan Faves" class="Link" href="#">$5 Fan Faves</a></li>
                            <li><a text="Indian Cinema" class="Link" href="#">Indian Cinema</a></li>
                            <li><a text="Asian-Pacific Cinema" class="Link" href="#">Asian-Pacific Cinema</a></li>
                            <li><a text="Pantallas AMC" class="Link" href="#">Pantallas AMC</a></li>
                            <li><a text="Film Festivals" class="Link" href="#">Film Festivals</a></li>
                            <li><a text="Fathom Events" class="Link" href="#">Fathom Events</a></li>
                            <li><a text="The Metropolitan Opera" class="Link" href="#">The Metropolitan Opera</a></li>
                            <li><a text="Sensory Friendly Films" class="Link" href="#">Sensory Friendly Films</a></li>
                            <li><a text="Groups &amp; Events" class="Link" href="#">Groups &amp; Events</a></li>
                        </ul>
                    </div>
                    <div class="section">
                        <h3 class="mb-3">More</h3>
                        <ul class="u-listUnstyled" role="navigation">
                            <li><a text="AMC Stubs" class="Link" href="#">AMC Stubs</a></li>
                            <li><a text="Offers &amp; Promotions" class="Link" href="#">Offers &amp; Promotions</a></li>
                            <li><a text="Gift Cards" class="Link" href="#">Gift Cards</a></li>
                            <li><a text="Mobile App" class="Link" href="#">Mobile App</a></li>
                            <li><a text="AMC Scene" class="Link" href="#">AMC Scene</a></li>
                            <li><a text="American Film Institute" class="Link" href="#">American Film Institute</a></li>
                            <li><a text="Assistive Moviegoing" class="Link" href="#">Assistive Moviegoing</a></li>
                            <li><button class="Link Btn--noStyle" type="button">Request Refund</button></li>
                            <li><a text="Business Clients" class="Link" href="#">Business Clients</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="flex items-center justify-start flex-wrap">
                <p class="opacity-75 text-sm md:mb-0 mb-3">&copy; @php echo date('Y'); @endphp By Edyta Jordan. Inspiration:
                    <a href="https://www.amctheatres.com/movie-theatres/salt-lake-city/amc-west-jordan-12" target="_blank" class="text-brand">AMC West Jordan 12 - UTAH</a> &
                    <a href="https://www.imdb.com/title/tt0349683/" target="_blank" class="text-brand">IMDB</a> </p>
            </div>

        </footer>
    </div>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/script.js') }}"></script>
    @yield('scripts')


</body>
</html>
