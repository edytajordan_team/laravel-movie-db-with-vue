<div id="top-bar" class="">
    <span class="m-0 p-0 fixed top-0 h-4px gradient-orange-to-pink-via-red inline-block w-full"></span>
    <!-- Top Bar -->
    <header class="top-bar pt-4px">
        <!-- Brand -->
        <a class="text-gray-700 flex items-center text-3xl font-light" href="{{ route('dashboard') }}">
            <span>CINE</span>
            <img src="{{ mix('images/film.svg') }}"/>
            <span>A (Admin)</span>
        </a>

        <!-- Menu Toggler -->
        <button type="button" class="menu-toggler ml-7 la la-bars" id="menuToggler" onclick="toggleMenu()"></button>

        <!-- Right -->
        <div class="flex items-center ml-auto">

            <!-- Dark Mode -->
            <dark-mode-toggler></dark-mode-toggler>

            <!-- User Menu -->
            <user-menu :username="'{{ Auth()->user()->name ?? 'Unknown' }}'"></user-menu>
        </div>
    </header>
</div>

<!-- Main Left Menu -->
<main-menu :route="'{{ Request::path() }}'"></main-menu>
