@extends('layouts.app')

@section('content')
    <h2 class="mb-5">Add/Edit Showtimes for '{{ $movie->title }}'</h2>
    <div class="bg-white shadow-sm rounded-md p-6">
        <form method="POST" action="{{ route('movies-showtimes.store') }}" enctype="multipart/form-data" class="mb-7 clearfix">
        @csrf

            @if($errors->any())
                <div class="rounded-md bg-red-50 p-4">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: solid/x-circle -->
                            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3">
                            <h3 class="text-sm font-medium text-red-800">
                                There were following with your submission
                            </h3>
                            <div class="mt-2 text-sm text-red-700">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="pt-5">
                <div class="clearfix">
                    <span class="mr-2 inline-block">
                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                            <input type="date" name="date" class="border-gray-300 rounded-md">
                        </div>
                    </span>
                    <span class="mr-2 inline-block">
                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                            <select id="time"
                                    name="time"
                                    class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
                                <option value="11:00 AM">11:00 AM</option>
                                <option value="12:00 PM">12:00 PM</option>
                                <option value="1:00 PM">1:00 PM</option>
                                <option value="2:00 PM">2:00 PM</option>
                                <option value="3:00 PM">3:00 PM</option>
                                <option value="4:00 PM">4:00 PM</option>
                                <option value="5:00 PM">5:00 PM</option>
                                <option value="6:00 PM">6:00 PM</option>
                                <option value="7:00 PM">7:00 PM</option>
                                <option value="8:00 PM">8:00 PM</option>
                            </select>
                        </div>
                    </span>
                    <button type="submit"
                            class="ml-2 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm
                        text-sm font-medium rounded-md text-white bg-brand hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Save
                    </button>
                    <input type="hidden" name="movie_id" value="{{$movie->id}}">
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <div class="space-y-8 sm:space-y-5">
        <div>
            @foreach($showtimes as $showtime)
                <span class="inline-flex rounded-lg items-center py-0.5 pl-2.5 pr-1 text-sm font-medium bg-indigo-100 text-indigo-700 m-1 p-2">
                    {{ $showtime->date }}&nbsp;&nbsp;<span class="font-bold">{{ $showtime->time }}</span>
                            <button type="button" class="flex-shrink-0 ml-0.5 h-4 w-4 rounded-full inline-flex items-center justify-center text-indigo-400
                            hover:bg-indigo-200 hover:text-indigo-500 focus:outline-none focus:bg-indigo-500 focus:text-white">
    <span class="sr-only">Remove large option</span>
    <svg class="h-2 w-2" stroke="currentColor" fill="none" viewBox="0 0 8 8">
      <path stroke-linecap="round" stroke-width="1.5" d="M1 1l6 6m0-6L1 7" />
    </svg>
  </button>
                    </span>
            @endforeach
        </div>
    </div>
    </div>
@endsection