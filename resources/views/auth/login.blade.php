@extends('layouts.blank-background')

@section('title')

@endsection

@section('content')

    <div class="min-h-screen flex flex-col justify-center sm:px-6 lg:px-8">
        <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div class="gradient-pink-to-orange-via-red p-1px sm:rounded-lg">
                <div class="bg-gray-900 py-8 px-4 shadow-xl sm:rounded-lg sm:px-10">
                    <h2 class="mt-6 text-center text-3xl text-gray-500 text-xs uppercase">
                        Sign In
                    </h2>
                    <form method="POST">
                        @csrf
                        <div>
                            <label for="email" class="block font-medium text-gray-500 text-xs uppercase tracking-wide">
                                Email
                            </label>
                            <div class="mt-1 rounded-md shadow-sm">
                                <input name="email" type="email" required
                                       class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none
                                       focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                            </div>
                        </div>
                        <div class="mt-6">
                            <label for="password" class="block font-medium text-gray-500 text-xs uppercase tracking-wide">
                                Password
                            </label>
                            <div class="mt-1 rounded-md shadow-sm">
                                <input name="password" type="password" required
                                       class="appearance-none block w-full px-3 py-2 border border-gray-300
                                   rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                            </div>
                        </div>
                        <div class="mt-6">
                            @if($errors->any())
                                <h4 class="pb-10 text-center">{{$errors->first()}}</h4>
                            @endif
                            <span class="block w-full rounded-md shadow-sm">
            <button type="submit"
                    class="w-full flex justify-center py-2 px-4 text-sm font-medium rounded-md text-white
                    gradient-pink-to-orange-via-red focus:outline-none
                    transition duration-150 ease-in-out">
              Sign in
            </button>
          </span>
                        </div>
                    </form>
                    <div class="text-sm text-center leading-5 mt-8">
                        <a href="/password/reset"
                           class="font-medium hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150 text-gray-500">
                            Forgot your password?
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
