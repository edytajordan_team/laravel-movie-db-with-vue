const defaultTheme = require('tailwindcss/defaultTheme');
const screenDebugger = require('tailwindcss-debug-screens');
const tailwindForms = require('@tailwindcss/forms');

module.exports = {
    darkMode: 'class', // or false or 'media' or 'class'
    theme: {
        container: {
            center: true,
            padding: '1rem',
        },
        maxHeight: {
            ...defaultTheme.maxHeight,
            0: '0px',
        },
        rotate: {
            ...defaultTheme.rotate,
            315: '315deg',
        },
        debugScreens: {
            position: ['bottom', 'right'],
        },
        extend: {
            fontFamily: {
                body: ['Nunito Sans', 'sans-serif'],
                heading: ['Nunito', 'sans-serif'],
                sans: [
                    'Inter var', ...defaultTheme.fontFamily.sans,
                    'fieldwork', ...defaultTheme.fontFamily.sans,
                    'Barlow',
                    'sans-serif',
                ],
                alt: [
                    'Poppins',
                    'sans-serif',
                ],
                serif: [
                    'Georgia',
                    'Cambria',
                    '"Times New Roman"',
                    'Times',
                    'serif',
                ],
                mono: [
                    'Menlo',
                    'Monaco',
                    'Consolas',
                    '"Liberation Mono"',
                    '"Courier New"',
                    'monospace',
                ],
            },
            fontSize: {
                '4xl': '2rem',
                'line-height': '3rem',
            },
            boxShadow: {
                DEFAULT: '1px 1px 5px 0 rgba(0, 0, 0, 0.16)',
            },
            colors: {
                'primary': '#4328d9',
                'primary-light': '#A65FEC',
                'secondary': '#ff9b05',
                'primary-dark': '#3e01a9',
                'secondary-dark': '#f87171',
                'secondary-light': '#f3d4ea',

                transparent: 'transparent',

                black: '#0d0d0d',
                white: '#fff',
                brand: '#ff5900',
            },
            spacing: {
                '1/1': '100%',
                '3/4': '75%',
                '9/16': '56.25%',
                px: '1px',
                '1px': '1px',
                '2px': '2px',
                '3px': '3px',
                '4px': '4px',
                '5px': '5px',
            },
            borderRadius: {
                none: '0',
                sm: '0.125rem',
                default: '0.25rem',
                lg: '0.5rem',
                full: '9999px',
                xl: '10px',
            },
            borderWidth: {
                default: '1px',
                '0': '0',
                '2': '2px',
                '4': '4px',
                '8': '8px',
            },
            boxShadow: {
                default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
                lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
                xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
                '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
                inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
                outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
                none: 'none',
            },
            letterSpacing: {
                tighter: '-0.05em',
                tight: '-0.025em',
                normal: '0',
                wide: '0.025em',
                wider: '0.05em',
                widest: '0.1em',
            },
            lineHeight: {
                none: '1',
                tight: '1.25',
                snug: '1.375',
                normal: '1.5',
                relaxed: '1.625',
                loose: '2',
            },
            zIndex: {
                auto: 'auto',
                'negative': '-1',
                '0': '0',
                '10': '10',
                '20': '20',
                '30': '30',
                '40': '40',
                '50': '50',
            },
        },
    },
    variants: {
        extend: {
            zIndex: ['responsive'],
        },
    },
    plugins: [
        screenDebugger, tailwindForms,
    ],
};
