<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert admin  user
        DB::table('movies')->insertOrIgnore([
            'title' => '30 Yards',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '1.jpg',
            'trailer' => '',
            'rating' => 'PG',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'Utah - New Season',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '2.jpg',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'Her Majesty',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '3.jpg',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'The Rumor',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '4.jpg',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'White Smoke',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '5.jpg',
            'trailer' => '',
            'rating' => 'PG',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'Mr. Brooklyn 2',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '6.jpg',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'Lost Man',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '7.jpg',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'Mars',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                                <p>Aenean ut facilisis sapien, non maximus diam. Nulla laoreet porttitor urna vel malesuada. In venenatis lorem eu nisl sollicitudin, vel varius ligula faucibus. 
                                Praesent nec metus non elit gravida vulputate non sed dui. Suspendisse sed rutrum ex. Nam malesuada sapien lobortis tristique rhoncus. </p>',
            'poster' => '8.webp',
            'trailer' => '',
            'rating' => 'R',
            'featured' => false,
        ]);

        DB::table('movies')->insertOrIgnore([
            'title' => 'King Arthur',
            'description' => '<p>A demystified take on the tale of King Arthur and the Knights of the Round Table. </p>',
            'poster' => '9.jpg',
            'trailer' => 'video.mp4',
            'rating' => 'PG-13',
            'featured' => true,
        ]);
    }
}
