<?php
namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MovieShowtimesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = Movie::all();
        if ($movies->count() === 0) {
            $this->command->info('There are no movies, so no showtimes will be added');
            return;
        }

        $dates = [
            '2021-07-07',
            '2021-07-08',
            '2021-07-09',
        ];
        $h = 11; //todo showtimes will start at 11am and end at 8pm
        $showtimes = [];
        while ($h < 21) {
            $key = date('H:i', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $value = date('h:i A', strtotime(date('Y-m-d') . ' + ' . $h . ' hours'));
            $showtimes[] = $value;
            $h++;
        }

        for($i=0; $i < $movies->count(); $i++) {
            foreach ($dates as $date) {
                foreach ($showtimes as $showtime) {
                    DB::table('movie_showtimes')->insertOrIgnore([
                        'movie_id' => $movies[$i]->id,
                        'date' => $date,
                        'time' => $showtime,
                    ]);
                }
            }
        }
    }
}
