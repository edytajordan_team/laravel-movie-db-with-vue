<?php
namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MovieReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = Movie::all();
        $num_of_reviews = 25;

        if ($movies->count() === 0) {
            $this->command->info('There are no movies, so no reviews will be added');
            return;
        }

        for($i=0; $i<= $num_of_reviews; $i++){
            DB::table('movie_reviews')->insertOrIgnore([
                'movie_id' => $movies->random()->id,
                'review' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor sem, fermentum bibendum orci quis, fermentum bibendum nisi. 
                                Integer vel commodo nisl, at vestibulum nibh. Mauris ac nisi vitae tortor porta convallis condimentum in lorem. Quisque porttitor ut erat at malesuada. 
                                Etiam vehicula lorem ac mi commodo, eu consectetur nisl cursus. </p>
                            ',
            ]);
        }
    }
}
