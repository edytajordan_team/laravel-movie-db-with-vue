# The Laravel Movie DB
Welcome to the Laravel Movie Database.

### Getting Started

PHP version: 8.x

Vue.js 3 - https://v3.vuejs.org/guide/introduction.html

TailwindCSS 2.x - https://tailwindcss.com/docs

TailwindUI - https://tailwindui.com/documentation (you will need a login to access the license and paid components/assets).

Laravel MIX 6.x - https://laravel-mix.com/docs/6.0/installation

Make sure you've got Valet (or any other local dev tool of your choice) installed and that you've linked your repo to a local URL. Configure that URL in your `.env`.

### Run commands

Run `composer install`.

Next, run `npm install && npx mix `.

```
PLEASE NOTE: Laravel MIX 6.0+

Laravel Movie DB is using Laravel MIX 6 (https://laravel-mix.com/docs/6.0/upgrade) and due to that update, scripts in `package.json` are a bit different than what you may be used to.

In order to `watch` or compile assets (Tailwind and Vue components) for `dev` or `production` you will need to use the following commands:

`npx mix` and/or `npx mix watch`. Please refer to official Laravel MIX docs and scripts in package.json for more info.
```

Edit your `.env` file and add your local url (you will need it for Postman Collection). Add database connection as well. 

Next, run `php artisan migrate`.

Next, run `php artisan db:seed`.

If all went well, you should be able to login (/login) with email: `admin@cinemaadmin.test` & password: `ILoveMovies`. 

### Next steps
I have attached Postman Collection and Environment exports for your convenience. See at `/docs/Postman`.

I have also attached in the same folder, a PDF file with screenshots of what you should see when the set up is executed successfully.

